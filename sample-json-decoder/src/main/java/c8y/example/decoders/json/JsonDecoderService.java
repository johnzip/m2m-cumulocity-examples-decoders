/*
 * Copyright © 2019 Software AG, Darmstadt, Germany and/or its licensors
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package c8y.example.decoders.json;

import c8y.example.decoders.json.util.Measurement;
import com.cumulocity.microservice.customdecoders.api.model.DecoderResult;
import com.cumulocity.microservice.customdecoders.api.service.DecoderService;
import com.cumulocity.model.idtype.GId;
import com.cumulocity.rest.representation.inventory.ManagedObjects;
import com.cumulocity.sdk.client.measurement.MeasurementApi;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
@Getter
@Setter
@Slf4j
public class JsonDecoderService implements DecoderService {

    public final static HashMap<String, String> mapping = new HashMap<>();
    static {
        mapping.put("temp",         "c8y_DTV.temperature.value");
        mapping.put("humidity",     "c8y_DTV.humidity.value");
        mapping.put("dewPoint",     "c8y_DTV.dewPoint.value");
    }


    private MeasurementApi measurementApi;

    @Autowired
    public JsonDecoderService(MeasurementApi measurementApi) {
        this.measurementApi = measurementApi;
    }

    @Override
    public DecoderResult decode(String payloadToDecode, GId sourceDeviceId, Map<String, String> inputArguments) {

        log.debug("Decoding payload {}.", payloadToDecode);
        DecoderResult decoderResult = new DecoderResult();

        JSONObject input;
        try {
            input = (JSONObject)new JSONParser().parse(payloadToDecode);
        } catch (ParseException p) {
            return decoderResult.setAsFailed(p.getMessage());
        }

        Measurement m = new Measurement();
        m.setSource(ManagedObjects.asManagedObject(sourceDeviceId));
        m.setType("c8y_DTV");
        m.setDateTime(new DateTime( (long) input.get("ts")));
        JSONObject values = (JSONObject) input.get("values");
        for(Map.Entry<String, String> entry: mapping.entrySet())
            m.set(entry.getValue(), values.get(entry.getKey()));
        measurementApi.create(m);

        log.debug("Finished decoding byte values");
        return decoderResult;
    }
}
