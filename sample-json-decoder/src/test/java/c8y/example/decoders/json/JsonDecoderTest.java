package c8y.example.decoders.json;

import c8y.example.decoders.json.util.Measurement;
import c8y.example.decoders.json.util.MeasurementTest;
import com.cumulocity.microservice.customdecoders.api.exception.DecoderServiceException;
import com.cumulocity.microservice.customdecoders.api.model.DecoderInputData;
import com.cumulocity.microservice.customdecoders.api.model.DecoderResult;
import com.cumulocity.model.idtype.GId;
import com.cumulocity.rest.representation.measurement.MeasurementRepresentation;
import com.cumulocity.sdk.client.measurement.MeasurementApi;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class JsonDecoderTest {

    private final String msg = "" +
            "{" +
                "\"ts\":1550012651000," +
                "\"values\":{" +
                    "\"temp\":23.43," +
                    "\"humidity\":34.43," +
                    "\"dewPoint\":12.01" +
                "}" +
            "}";
    private final DecoderInputData inputData = new DecoderInputData();
    private final GId id = new GId("12345");

    private MeasurementApi measurementApi = mock(MeasurementApi.class);
    private DecoderResult decoderResult;

    private ArrayList<MeasurementRepresentation> measurementsCreated = new ArrayList<>();

    @Before
    public void setUp() throws DecoderServiceException {
        when(measurementApi.create(any(MeasurementRepresentation.class))).thenAnswer(new Answer<Object>() {
            @Override
            public MeasurementRepresentation answer(InvocationOnMock invocation) {
                MeasurementRepresentation m = (MeasurementRepresentation) invocation.getArguments()[0];
                measurementsCreated.add(m);
                return m;
            }
        });

        inputData.setValue(msg);
        inputData.setSourceDeviceId(id.getValue());
        decoderResult = new JsonDecoder(new JsonDecoderService(measurementApi)).decodeWithJSONInput(inputData);
    }

    @Test
    public void validateMeasurements() {
        assertEquals(1, measurementsCreated.size());
        Measurement measurement = (Measurement)measurementsCreated.get(0);
        for (String path: JsonDecoderService.mapping.values()) {
            assertEquals(MeasurementTest.mapping.get(path), measurement.get(path));
        }
    }


    @Test
    public void validateDecoderResult() {
        assertNull(decoderResult.getAlarms());
        assertNull(decoderResult.getEvents());
        assertNull(decoderResult.getDataFragments());
        assertNull(decoderResult.getMeasurements());
    }

}