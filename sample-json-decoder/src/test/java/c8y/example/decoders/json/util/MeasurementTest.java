package c8y.example.decoders.json.util;

import com.cumulocity.model.idtype.GId;
import com.cumulocity.rest.representation.inventory.ManagedObjects;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@Slf4j
public class MeasurementTest {

    public final static HashMap<String, Double> mapping = new HashMap<>();
    static {
        mapping.put("c8y_DTV.temperature.value", 23.43);
        mapping.put("c8y_DTV.humidity.value", 34.43);
        mapping.put("c8y_DTV.dewPoint.value", 12.01);
    }

    @Test
    public void setAndGet() {
        Measurement m = new Measurement();
        m.setSource(ManagedObjects.asManagedObject(new GId("12345")));
        m.setType("c8y_DTV");
        m.setDateTime(new DateTime(0).toDateTime(DateTimeZone.UTC));
        for(Map.Entry<String, Double> entry: mapping.entrySet())
            m.set(entry.getKey(), entry.getValue());
        for(Map.Entry<String, Double> entry: mapping.entrySet())
            assertEquals(m.get(entry.getKey()),entry.getValue());
    }
}