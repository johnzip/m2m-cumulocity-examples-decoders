/*
 * Copyright © 2019 Software AG, Darmstadt, Germany and/or its licensors
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package c8y.example.decoders.base64.util;

import org.junit.Test;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class MessageTest {

    // private final static String MESSAGE_STR = "QbgUe0IF4UhAcAAA";
    private final static String MESSAGE_STR = "FwAAACEAAAADAAAA";
    // private final static String MESSAGE_STR_FLIPPED = "QjIKPUKha4VBc64U";
    private final static String MESSAGE_STR_FLIPPED = "LAAAAFAAAAAPAAAA";

    public static byte[] IntArray2ByteArray(int[] values){
        ByteBuffer buffer = ByteBuffer.allocate(4 * values.length);
    
        for (int value : values){
            buffer.order(ByteOrder.LITTLE_ENDIAN).putInt(value);
        }
        //ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(value).array();
    
        return buffer.array();
    }

    @Test
    public void toByteArray() {
        byte[] expectedResult = IntArray2ByteArray(new int[]{(int)23, (int)33, (int)3});
        byte[] result = Message.toByteArray(MESSAGE_STR);
        assertEquals(expectedResult.length, result.length);
        for(int i = 0; i < result.length; i++) {
            System.out.printf("i: %d, expected: %d, result: %d\n", i, expectedResult[i], result[i]);
            assertEquals(expectedResult[i], result[i]);
        }
    }

    @Test
    public void constructorTest() {
        Message msg = new Message(MESSAGE_STR);

        assertEquals(23, msg.getTemperature());
        assertEquals(33, msg.getHumidity());
        assertEquals(3, msg.getDewPoint());
    }

    @Test
    public void constructorTestFlipped() {
        Message msg = new Message(MESSAGE_STR_FLIPPED);

        assertEquals(44, msg.getTemperature());
        assertEquals(80, msg.getHumidity());
        assertEquals(15, msg.getDewPoint());
    }
}