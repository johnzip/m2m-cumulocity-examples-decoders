package c8y.example.decoders.base64;

import c8y.Position;
import c8y.example.decoders.base64.util.Measurement;
import c8y.example.decoders.base64.util.Message;
import com.cumulocity.microservice.customdecoders.api.model.DecoderInputData;
import com.cumulocity.microservice.customdecoders.api.model.DecoderResult;
import com.cumulocity.model.event.CumulocitySeverities;
import com.cumulocity.model.idtype.GId;
import com.cumulocity.rest.representation.alarm.AlarmRepresentation;
import com.cumulocity.rest.representation.event.EventRepresentation;
import com.cumulocity.rest.representation.inventory.ManagedObjectRepresentation;
import com.cumulocity.rest.representation.measurement.MeasurementRepresentation;
import com.cumulocity.sdk.client.inventory.InventoryApi;
import com.cumulocity.sdk.client.measurement.MeasurementApi;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class base64DecoderTest {

    private final String msg_str = "FwAAACEAAAADAAAA";
    private final DecoderInputData inputData= new DecoderInputData();
    private final Message msg = new Message(msg_str);
    private final GId id = new GId("12345");

    private MeasurementApi measurementApi = mock(MeasurementApi.class);
    private InventoryApi inventoryApi = mock(InventoryApi.class);
    private DecoderResult decoderResult;

    private ArrayList<MeasurementRepresentation> measurementsCreated = new ArrayList<>();
    private ArrayList<ManagedObjectRepresentation> inventoriesUpdated = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        when(measurementApi.create(any(MeasurementRepresentation.class))).thenAnswer(new Answer<MeasurementRepresentation>() {
            @Override
            public MeasurementRepresentation answer(InvocationOnMock invocation) {
                MeasurementRepresentation m = (MeasurementRepresentation) invocation.getArguments()[0];
                measurementsCreated.add(m);
                return m;
            }
        });

        when(inventoryApi.update(any(ManagedObjectRepresentation.class))).thenAnswer(new Answer<ManagedObjectRepresentation>() {
            @Override
            public ManagedObjectRepresentation answer(InvocationOnMock invocation) {
                ManagedObjectRepresentation mo = (ManagedObjectRepresentation) invocation.getArguments()[0];
                inventoriesUpdated.add(mo);
                return mo;
            }
        });

        inputData.setValue(msg_str);
        inputData.setSourceDeviceId(id.getValue());
        decoderResult = new base64Decoder(new base64DecoderService(measurementApi, inventoryApi)).decodeWithJSONInput(inputData);
    }

    @Test
    public void validateMeasurements(){
        assertEquals(1, measurementsCreated.size());
        Measurement measurement = (Measurement)measurementsCreated.get(0);
        assertEquals(id.getValue(), measurement.getSource().getId().getValue());
        assertEquals("c8y_DTV", measurement.getType());
        assertEquals(msg.getTemperature(), measurement.get("c8y_DTV.temperature.value"));
        assertEquals(msg.getHumidity(), measurement.get("c8y_DTV.humidity.value"));
        assertEquals(msg.getDewPoint(), measurement.get("c8y_DTV.dewPoint.value"));
    }

}