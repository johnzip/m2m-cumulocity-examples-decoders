/*
 * Copyright © 2019 Software AG, Darmstadt, Germany and/or its licensors
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package c8y.example.decoders.base64;

import c8y.Position;
import c8y.example.decoders.base64.util.Measurement;
import c8y.example.decoders.base64.util.Message;
import com.cumulocity.microservice.customdecoders.api.model.DecoderResult;
import com.cumulocity.microservice.customdecoders.api.service.DecoderService;
import com.cumulocity.model.event.CumulocitySeverities;
import com.cumulocity.model.idtype.GId;
import com.cumulocity.rest.representation.alarm.AlarmRepresentation;
import com.cumulocity.rest.representation.event.EventRepresentation;
import com.cumulocity.rest.representation.inventory.ManagedObjectRepresentation;
import com.cumulocity.rest.representation.inventory.ManagedObjects;
import com.cumulocity.sdk.client.inventory.InventoryApi;
import com.cumulocity.sdk.client.measurement.MeasurementApi;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;


@Component
@Slf4j
public class base64DecoderService implements DecoderService {


    private final MeasurementApi measurementApi;
    private final InventoryApi inventoryApi;

    @Autowired
    public base64DecoderService(MeasurementApi measurementApi, InventoryApi inventoryApi) {
        this.measurementApi = measurementApi;
        this.inventoryApi = inventoryApi;
    }

    @Override
    public DecoderResult decode(String payloadToDecode, GId sourceDeviceId, Map<String, String> inputArguments) {

        log.debug("Decoding payload {}.", payloadToDecode);
        DecoderResult decoderResult = new DecoderResult();
        Message msg;
        try {
            msg = new Message(payloadToDecode);
        } catch (IllegalArgumentException e) {
            return decoderResult.setAsFailed(e.getMessage());
        }

        measurementApi.create(createMeasurement(sourceDeviceId, msg));

        log.debug("Finished decoding base64 values");
        return decoderResult;

    }

    private Measurement createMeasurement(GId sourceId, Message msg) {
        Measurement m = new Measurement();
        m.setSource(ManagedObjects.asManagedObject(sourceId));
        m.setType("c8y_DTV");
        m.setDateTime(new DateTime()); //1970-01-01T01:00:00.000+01:00
        m.set("c8y_DTV.temperature.value", msg.getTemperature());
        m.set("c8y_DTV.humidity.value", msg.getHumidity());
        m.set("c8y_DTV.dewPoint.value", msg.getDewPoint());
        return m;
    }

}
