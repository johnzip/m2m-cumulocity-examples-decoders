/*
 * Copyright © 2019 Software AG, Darmstadt, Germany and/or its licensors
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package c8y.example.decoders.base64.util;

import c8y.Position;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Base64;

@Getter
@Setter
@Slf4j
public class Message {

    public static final int MESSAGE_LENGTH = 16; // 12 bytes represented as 16 chars in Base64

    private int temperature;  // 4 bytes for each integer number
    private int humidity;
    private int dewPoint;

    public Message(String base64Message) {
        if (base64Message.length() < MESSAGE_LENGTH) {
            log.error("Message too short: " + base64Message);
            throw new IllegalArgumentException("Message too short: " + base64Message);
        }
        else if (base64Message.length() > MESSAGE_LENGTH)
            log.warn("Message too long: " + base64Message);

        byte[] msgBytes = Base64.getDecoder().decode(base64Message);
        ByteBuffer byteBuffer = ByteBuffer.wrap(msgBytes, 0, 12);

        temperature = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();
        humidity = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();
        dewPoint = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    public static byte[] IntArray2ByteArray(int[] values){
        ByteBuffer buffer = ByteBuffer.allocate(4 * values.length);
    
        for (int value : values){
            buffer.order(ByteOrder.LITTLE_ENDIAN).putInt(value);
        }
        //ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(value).array();
    
        return buffer.array();
    }
    
    static byte[] toByteArray(String base64Message){
        byte[] msgBytes = Base64.getDecoder().decode(base64Message);
        //System.out.printf("base64 string: %s\n", base64Message);
        //System.out.printf("length of msgBytes: %d\n", msgBytes.length);
        ByteBuffer byteBuffer = ByteBuffer.wrap(msgBytes, 0, 12);

        int value1 = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();
        int value2 = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();
        int value3 = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();

        //System.out.printf("Integers: %d, %d, %d\n", value1, value2, value3);
        return IntArray2ByteArray(new int[]{value1, value2, value3});
    }
}
