Cumulocity Custom Payload Processor examples
--------------------------------------------

This repository contains example applications created using Cumulocity SDK. For more information on [Cumulocity] [1] visit [https://www.softwareag.cloud/site/dev-center/cumulocity-iot.html#/] [1].

Examples are based on repository [https://bitbucket.org/m2m/cumulocity-examples/src/develop/]

Command to build project:

$ mvn -s settings.xml clean install